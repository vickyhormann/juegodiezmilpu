package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import modelo.Dado;

class DadoTest {

	@Test
	void testContrusctorCorrectoEnCara0() {
		Dado dado = new Dado();
		Assert.assertTrue(dado.getCara().equals(0));
	}
	
	@Test
	void valoresEntre1y6Test() {
		for (int i=0; i<=100; i++) {
		Dado dado = new Dado();
		dado.darValorAleatorio();
		
		Assert.assertTrue(dado.getCara()<=6 && dado.getCara()>=1);
		System.out.println(dado.getCara());}
	}

}

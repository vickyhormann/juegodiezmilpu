package controlador;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modelo.Cinco;
import modelo.Dado;
import modelo.Escalera;
import modelo.Jugador;
import modelo.Triple1;
import modelo.Triple2;
import modelo.Triple3;
import modelo.Triple4;
import modelo.Triple5;
import modelo.Triple6;
import modelo.Uno;
import vista.IVista;
import vista.VistaConsola;

public class ControladorJuego {

	private List<Jugador> jugadores = new ArrayList();
	private List<Dado> dados;
	private IVista iVista;
	private boolean jugar;
	private final Integer ganador = 10000;
	private final Integer minimo = 650;
	private final Integer cantDados = 5;
	private boolean terminado = false;
	
	public boolean chequearGanador() {
		boolean b = false;
		for (Jugador jugador : jugadores) {
			if (jugador.informarPuntaje() >= ganador) {
				b = true;
			}
		}
		return b;
	}
	
	
	public List<Dado> getDados(){
		return dados;}
	
	
	public ControladorJuego(IVista iVista) {
		this.iVista = iVista;
		}
	
	public void iniciar() {
		iVista.menuConfiguracion();
	}
	
	public String listarJugadores () {
		String s = "";
		for (Jugador jugador : jugadores) {
			s = s + jugador.getNombre() + "\n" ;
		}
		return s;
	}
	
	public String listarJugadoresPuntos () {
		String s = "";
		for (Jugador jugador : jugadores) {
			s = s + jugador.getNombre() + " Puntos " + jugador.informarPuntaje() + "\n" ;
		}
		return s;
	}
	
	
	public void inicializarDados() {
		dados = new ArrayList();
		for (int i = 0; i < cantDados; i++) {
			Dado dado = new Dado();
			dados.add(dado);
		}
	}
	
	public Integer cantidadDados () {
		Integer c = 0;
		for ( Dado dado : dados) {
			if (dado.getEstado()) {
				c = c+1;
			}
		}
		return c; 
	}
	
	public void iniciarRonda() {
		for (Jugador jugando : jugadores) {
			jugando.setEstado(true);
			jugar = true;
			inicializarDados();
			jugando.resetearParciales();
			do {
				jugando.setPuntosTiro(0);
				iVista.menuJugando();
				jugando.actualizarParciales(getPuntosTiro());
			}while (jugando.getPuntosTiro() > 0 && jugar);
			jugando.setEstado(false);
		}
		iVista.mostrarTotales();
	}
	
	
	public Integer getPuntosTiro() {
		Integer puntos = 0;
		for (Jugador jugador : jugadores) {
			if (jugador.getEstado()) {
				puntos = jugador.getPuntosTiro();
	}}
		return puntos;}
	
	public void quedarse() {
		if (actualizarPuntosJugador(jugadorJugando())) {
			jugar = false;
	} else {
		iVista.mostrarMensaje("Debes sumar 650 puntos la primera vez para poder quedarte");
		iVista.menuJugando();}
	}

	
	public boolean actualizarPuntosJugador (Jugador jugador) {
		if ((jugador.informarPuntaje() == 0 && jugador.informarParciales() >= minimo )|| jugador.informarPuntaje() > 0) {
			jugador.sumarPuntos(jugador.informarParciales());
			return true;
		} else {
			return false;
		}	
	}
	
	
	public boolean chequearJugadores () {
		if (jugadores.size() >= 2) {
			return true;}
		else {
			return false;}
	}
	
	public String tuTurno() {
		if (cantidadDados() == 0) {
			inicializarDados();
		}
		Jugador jugando = jugadorJugando();
		jugando.setEstado(true);
		dados = jugando.tirar(dados);
		jugando.setPuntosTiro(puntosConseguidos());
		return mostrarDados();
	}
	
	public void iniciarJuego() {
		do {
			iniciarRonda();
		} while (!chequearGanador() && !terminado);
		informarGanador();
	}
	
	public void informarGanador() {
		for (Jugador jugador : jugadores) {
			if(jugador.informarPuntaje() >= ganador) {
				System.out.println("GANADOR " + jugador.getNombre() + " con " + jugador.informarPuntaje() + " puntos");
			}
		}
	}
	
	public String mostrarDados() {
		String s = "";
		for (Dado dado : dados) {
			s = s + dado.getCara() + "\n";
		}
		return s;
	}
	
	public void agregarJugador(String nombre) {
		Jugador nuevoJugador = new Jugador(nombre);
		jugadores.add(nuevoJugador);
	}
	
	public String nombreJugadorJugando() {
		String jugando = "";
		for (Jugador jugador : jugadores) {
			if (jugador.getEstado()) {
				jugando = jugador.getNombre();
			}
		}
		return jugando;
	}
	
	public Integer mostrarParcialesJugando () {
		Integer puntos = 0;
		for (Jugador jugador : jugadores) {
			if (jugador.getEstado()) {
				puntos = jugador.informarParciales();
	}}
		return puntos;}
	
	public Jugador jugadorJugando() {
		Jugador juega = new Jugador("");
		for (Jugador jugador : jugadores) {
			if (jugador.getEstado()) {
				juega = jugador;
			}
		}
		return juega;
	}
	
	public Integer puntosConseguidos () {
		Integer puntos = 0;
		Escalera escalera = new Escalera();
		Triple1 triple1 = new Triple1();
		Triple2 triple2 = new Triple2();
		Triple3 triple3 = new Triple3();
		Triple4 triple4 = new Triple4();
		Triple5 triple5 = new Triple5();
		Triple6 triple6 = new Triple6();
		Uno uno = new Uno();
		Cinco cinco = new Cinco();
		puntos = escalera.validarRegla(dados);
		if (puntos == 0) {
			puntos = triple1.validarRegla(dados)+ triple2.validarRegla(dados)+triple3.validarRegla(dados)+triple4.validarRegla(dados)+triple5.validarRegla(dados)+triple6.validarRegla(dados)+ uno.validarRegla(dados)+cinco.validarRegla(dados);
		}
		return puntos;
	}
	
	public void salir() {
		terminado  = true;
	}
}

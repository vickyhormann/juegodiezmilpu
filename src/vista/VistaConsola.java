package vista;

import java.util.Scanner;

import controlador.ControladorJuego;

public class VistaConsola implements IVista{
	private ControladorJuego miControlador;
	
	
	public VistaConsola() {
	  miControlador = new ControladorJuego(this);	
	}
		
	public void iniciarJuego() {
		miControlador.iniciar();
	}
	public void menuConfiguracion() {
		boolean jugar = false;
		int opcion = 0;
		do {
			limpiarConsola();
			System.out.println("BIENVENIDO AL DIEZ MIL");
			System.out.println();
			System.out.println("1: Agregar jugador");
			System.out.println();
			System.out.println("2: Listar jugadores");
			System.out.println();
			System.out.println("3: Comenzar a jugar");
			System.out.println();
			System.out.println("0: Salir del juego");
			System.out.println();
			System.out.println("Elija la opci�n deseada");
			Scanner reader = new Scanner(System.in);
			opcion = reader.nextInt();
			while (!validarEntrada(0,3,opcion)) {
				opcion = reader.nextInt();}
			switch (opcion)  {
				case 1 : {	System.out.println("Ingrese el nombre del nuevo jugador");
						Scanner entrada = new Scanner (System.in);
						String nombre = entrada.nextLine();
						miControlador.agregarJugador(nombre);
						break;
				}
				case 2 : { System.out.println("Lista de jugadores");
							System.out.println(miControlador.listarJugadores());
							break;
				}
				case 3 : { if (miControlador.chequearJugadores()) {
							System.out.println("Comenzando el juego...");
							miControlador.iniciarJuego();
							jugar = true;}
								else { System.out.println("Jugadores insuficientes, por favor agregue un nuevo jugador");
							}break;
				}
			}
		}while (!jugar && opcion!=0);
		if (opcion == 0) {
			miControlador.salir();
		}
			}
	
	@Override
	public void menuJugando() {
		boolean jugar = true;
		limpiarConsola();
		System.out.println("Es el turno de " + miControlador.nombreJugadorJugando());
			System.out.println("Puntos parciales: " + miControlador.mostrarParcialesJugando());
			System.out.println();
			System.out.println(" 1: Tirar los dados");
			System.out.println();
			System.out.println(" 2: Sumar los puntos");
			System.out.println();
			System.out.println("0: Finalizar juego");
			System.out.println();
			System.out.println("Elija la opci�n deseada");
			Scanner entrada = new Scanner (System.in);
			Integer opcion = entrada.nextInt();
			while (!validarEntrada(0,2,opcion)) {
				opcion = entrada.nextInt();
			}
			switch (opcion) {
			case 1 : { 	System.out.println(miControlador.tuTurno());
						System.out.println("Puntos conseguidos en este tiro: " + miControlador.getPuntosTiro());
						break;
			} 
			case 2 : { miControlador.quedarse();
			break;
			}
			case 0: {
				miControlador.salir();
			}
			}
			
		
	}
	
	public void mostrarMensaje(String mensaje) {
		System.out.println(mensaje);
	}
	
	public void mostrarTotales() {
		System.out.println(miControlador.listarJugadoresPuntos());
	}
	
	public boolean validarEntrada(Integer inicio, Integer fin, Integer opcion) {
		if (opcion > fin || opcion < inicio) {
			System.out.println("Ingrese una opcion v�lida");
			return false;
		}
		return true;
	}
	public void limpiarConsola() {
	
	}
	
	

}

package vista;

import modelo.Jugador;

public interface IVista {
	
	void menuConfiguracion ();
	
	void menuJugando();
	
	void mostrarTotales();
	
	void mostrarMensaje(String mensaje);
	
}

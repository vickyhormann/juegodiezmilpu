package modelo;

import java.util.List;

public abstract class Regla {
	
	public abstract Integer getPuntaje();
	
	public abstract Integer validarRegla(List<Dado> dados);
	
	public Integer cantDados(List<Dado> dados) {
		Integer c = 0;
		for ( Dado dado : dados) {
			if (dado.getEstado()) {
				c = c+1;
			}
		}
		return c;
	}
	
}

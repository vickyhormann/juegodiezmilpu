package modelo;

public class Dado {
	
	private Integer cara;
	private boolean estado;
	
	public Dado() {
		cara = 0;
		estado = true;
	}
	
	public void darValorAleatorio() {
		cara = (int) (Math.random()*6+1);
	}
	
	public void setCara (int i) {
		cara = i;
	}
	public Integer getCara() {
		return cara;
	}
	
	public boolean getEstado() {
		return estado;
	}
	
	public void cambiarEstado (boolean est) {
		estado = est; 
	}
}

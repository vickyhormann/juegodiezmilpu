package modelo;

import java.util.List;

public class Triple4 extends Regla {
	private Integer puntaje;

	public Triple4 () {
		puntaje = 400;
	}
	@Override
	public Integer getPuntaje() {	
		return puntaje;
	}

	@Override
	public Integer validarRegla(List<Dado> dados) {
		Integer c = 0;
		if (cantDados(dados) >= 3) {	
		for (Dado dado : dados) {
				if (dado.getCara() == 4) {
					c++;
				}
			}}
		if (c >= 3) {
			for (Dado dado : dados) {
				if (dado.getCara() == 4) {
					dado.cambiarEstado(false);
				}
			}
		return (getPuntaje());
	} else { return 0;}
	}
}

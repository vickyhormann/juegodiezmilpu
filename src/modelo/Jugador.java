package modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Jugador {
	
	private String nombre;
	private boolean	jugando;
	private Integer puntosParciales;
	private Integer puntosTotales;
	private Integer puntosTiro;
	
	
	public Jugador(String nombre) {
		this.nombre = nombre;
		puntosParciales = 0;
		puntosTotales = 0;
		puntosTiro = 0;
		jugando = false;
	}
	
	public void setPuntosTiro (Integer puntos) {
		puntosTiro = puntos;
	}
	
	public Integer getPuntosTiro () {
		return puntosTiro;
	}
	
	public List<Dado> tirar (List<Dado> dados) {
		List <Dado> tirados = new ArrayList();
		int i = 6;
		for (Dado dado : dados) {
			if (dado.getEstado()) {
			dado.darValorAleatorio();
			tirados.add(dado);
		}
		}
		return tirados;
	}
	
	public boolean getEstado() {
		return jugando;
	}
	
	public void setEstado(boolean estado) {
		jugando = estado;
	}
	
	public void sumarPuntos(Integer puntosParciales) {
		puntosTotales = puntosTotales + puntosParciales;
	}
	
	public Integer informarPuntaje() {
		return puntosTotales;
	}
	
	public Integer informarParciales() {
		return puntosParciales;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void resetearParciales() {
		puntosParciales = 0;
	}
	public void actualizarParciales (Integer puntos) {
		puntosParciales = puntosParciales + puntos;
	}
}

package modelo;

import java.util.List;

public class Triple2 extends Regla {
		private Integer puntaje;

		public Triple2 () {
			puntaje = 200;
		}
		@Override
		public Integer getPuntaje() {	
			return puntaje;
		}

		@Override
		public Integer validarRegla(List<Dado> dados) {
			Integer c = 0;	
			if (cantDados(dados) >= 3) {
			for (Dado dado : dados) {
					if (dado.getCara() == 2) {
						c++;
					}
				}}
			if (c >= 3) {
				for (Dado dado : dados) {
					if (dado.getCara() == 2) {
						dado.cambiarEstado(false);
					}
				}
			return (getPuntaje());
		} else { return 0;}
		}
}

package modelo;

import java.util.List;

public class Cinco extends Regla{
	private Integer puntaje;
	
	
	public Cinco() {
		puntaje = 50;
	}
	
	@Override
	public Integer getPuntaje() {
		// TODO Auto-generated method stub
		return puntaje;
	}

	@Override
	public Integer validarRegla(List<Dado> dados) {
		// TODO Auto-generated method stub
		Integer puntos = 0;
		for (Dado dado : dados) {
			if (dado.getEstado()) {
				if (dado.getCara() == 5) {
				puntos = puntos + getPuntaje();
				dado.cambiarEstado(false);
				}
			}
		}
		return puntos;
	}

}

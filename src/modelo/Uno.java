package modelo;

import java.util.List;

public class Uno extends Regla{
	private Integer puntaje;
	
	
	public Uno() {
		puntaje = 100;
	}
	
	@Override
	public Integer getPuntaje() {
		// TODO Auto-generated method stub
		return puntaje;
	}

	@Override
	public Integer validarRegla(List<Dado> dados) {
		// TODO Auto-generated method stub
		Integer puntos = 0;
		for (Dado dado : dados) {
			if (dado.getEstado()) {
				if (dado.getCara() == 1) {
					puntos = puntos + getPuntaje();
					dado.cambiarEstado(false);
				}
			}
		}
		return puntos;
	}
	

}

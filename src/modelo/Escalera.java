package modelo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Escalera extends Regla{
	
	private Integer puntaje;
	private Integer cantDadosNecesarios;
	
	public Escalera () {
		puntaje = 650;
		cantDadosNecesarios = 5;
	}
	
	@Override
	public Integer getPuntaje() {
		// TODO Auto-generated method stub
		return puntaje;
	}

	@Override
	public Integer validarRegla(List<Dado> dados) {
		boolean si = false;
		if (cantDados(dados) == cantDadosNecesarios) {
			List<Integer> numeros = new ArrayList();
			for (Dado dado : dados) {
				numeros.add(dado.getCara());
			if (numeros.containsAll(Arrays.asList(1,2,3,4,5))) {
				si = true; 
				}else {
					if (numeros.containsAll(Arrays.asList(2,3,4,5,6))) {
						si = true;
				}
			}
			}
	}
		if (si) {
			return (getPuntaje());
		}else {
			return 0;
		}
	}
}
		
	
	
	


